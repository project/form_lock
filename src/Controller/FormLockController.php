<?php

namespace Drupal\form_lock\Controller;

use Drupal\form_lock\Ajax\FormLockCommand;
use Drupal\form_lock\ServiceProvider\FormLockServiceProvider;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class FormLockController extends ControllerBase {


  /**
   * Form lock service.
   *
   * @var \Drupal\form_lock\ServiceProvider\FormLockServiceProvider
   */
  protected $lockService;

  public function __construct(FormLockServiceProvider $lock_service) {
    $this->lockService = $lock_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_lock')
    );
  }

   /**
   * Custom callback for the create lock route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function createLockCall(Request $request, $identity) {
    $response = new AjaxResponse();

    // Not lockable entity or entity creation.
    if (!is_object($this->lockService->isLockable($identity))) {
        $lockable = FALSE;
        $lock = FALSE;
      }
      else {
        $lockable = TRUE;
        $lock = $lockable ? $this->lockService->locking($identity, $this->currentUser()->id(), 300) : FALSE;
  
        // Render status messages from locking service.
        $response->addCommand(new InsertCommand('.margin-top-20.green', ['#type' => 'status_messages']));
      }
    $response->addCommand(new FormLockCommand($lockable, $lock));

    return $response;
  }
}