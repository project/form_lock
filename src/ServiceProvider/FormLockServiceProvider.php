<?php

namespace Drupal\form_lock\ServiceProvider;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

class FormLockServiceProvider extends ServiceProviderBase {

  use StringTranslationTrait;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module_handler service.
   */
  protected $moduleHandler;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   *   The database service.
   */
  protected $database;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   *  The current_user service.
   */
  protected $currentUser;

  /**
   * Time service.
   * 
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Entity Type Manager.
   * 
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Date Formatter.
   * 
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module Handler service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current_user service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(Connection $database, AccountProxyInterface $currentUser,
  ModuleHandlerInterface $moduleHandler, MessengerInterface $messenger, TimeInterface $time,
  EntityTypeManagerInterface $entityTypeManager, DateFormatterInterface $dateFormatter) {
      $this->database = $database;
      $this->moduleHandler = $moduleHandler;
      $this->currentUser = $currentUser;
      $this->messenger = $messenger;
      $this->time = $time;
      $this->entityTypeManager = $entityTypeManager;
      $this->dateFormatter = $dateFormatter;
  }

  /**
   * Fetch lock of a form with particular identity.
   * 
   * @param string $identity
   *   The form identifier.
   */
  public function fetchLock($identity) {
    $query = $this->database->select('form_lock', 'f');
    $query->leftJoin('users_field_data', 'u', '%alias.uid = f.uid');
    $query->fields('f')
      ->fields('u', ['name'])
      ->condition('f.locked_to', $this->time->getRequestTime(), '>')
      ->condition('f.identity', $identity);
    return $query->execute()->fetchObject();
  }

  /**
   * Tell who has locked.
   *
   *  @param object $lock
   *   The lock for a form.
   * 
   *  @return string
   *   String with the message.
   */
  public function displayLockOwner($lock) {
    $username = $this->entityTypeManager->getStorage('user')->load((int)$lock->uid);
    $remains = $this->time->getRequestTime() - $lock->locked_since;
    $date = $this->dateFormatter->formatInterval($this->time->getRequestTime() -  (300 - $remains));
   
    // @see https://stackoverflow.com/questions/5906686/php-time-remaining-until-specific-time-from-current-time-of-page-load
     $now = new \DateTime();
     $future_date = new \DateTime(date('m/d/Y H:i:s', $this->time->getRequestTime() - (300  - $remains)));
     $interval = $future_date->diff($now);
     $date = $interval->format("%i minutes, %s seconds");
    $message = $this->t('This form is being edited by the user @name and is therefore locked to prevent other users changes. This form is locked in place for @date.', [
        '@name' => $username->getDisplayName(),
        '@date' => $date,
      ]);
    return $message;
  }

  /**
   * Save locking into database.
   *
   * @param string $identity
   *   The form identifier.
   * @param int $uid
   *   The user uid.
   * @param int $locking_period
   *   The time until next locking is counted..
   *
   * @return bool
   *   The result of the merge query.
   */
  public function lockingSave($identity, $uid, $locking_period) {
    $result = $this->database->merge('form_lock')
      ->key([
        'identity' => $identity,
      ])
      ->fields([
        'identity' => $identity,
        'uid' => $uid,
        'locked_to' => $this->time->getRequestTime() + $locking_period,
        'locked_since' => $this->time->getRequestTime(),
        'locking_period' => $locking_period,
      ])
      ->execute();

    return $result;
  }

  /**
   * Delete form lock.
   * 
   * @param string $identity
   *   The form identifier.
   * 
   * @return bool
   *   The result of the delete query.
   */
  public function lockingDelete($identity) {
    $query = $this->database->delete('form_lock')
      ->condition('identity', $identity);
    $result = $query->execute();

    return $result;
  }

  /**
   * Lock functionality
   * 
   * @param string $identity
   *   Unique lock identified
   * @param interger $uid
   *   User ID to which form is locked.
   * @param integer $locking_period
   *   Duration for which form is to be locked.
   * @param boolean $quite
   *   Suppress message to be shown.
   */
  public function locking($identity, $uid, $locking_period, $quite = FALSE) {
    // Check locking status.
    $lock = $this->fetchLock($identity);

    // No lock yet.
    if ($lock == FALSE) {
      // Save locking into database.
      $this->lockingSave($identity, $uid, $locking_period);
      if (!$quite) {
        $this->messenger->addStatus($this->t('This form is now locked against simultaneous editing for @time min', 
        [
          '@time' => gmdate("i", $locking_period)
        ]));  
      }
    }
    else {
      // Currently locking by other user.
      if ($lock->uid != $uid) {
        // Send message.
        $message = $this->displayLockOwner($lock);
        $this->messenger->addStatus($message);

        // Return FALSE flag.
        return FALSE;
      }
      else {
        // Save locking into database.
        $this->lockingSave($identity, $uid, $locking_period);
        $this->messenger->addStatus($this->t('This form is now locked by you against simultaneous editing. This form will remain locked for @time mins', ['@time' => gmdate("i", $locking_period)]));

        // Send success flag.
        return TRUE;
      }
    }
  }

  function isLockable($identity) {
    $query = $this->database->select('form_lock', 'f');
    $query->leftJoin('users_field_data', 'u', '%alias.uid = f.uid');
    $query->fields('f')
      ->condition('f.identity', $identity)
      ->condition('f.locked_to', $this->time->getRequestTime(), '>');
    return $query->execute()->fetchObject();
  }

}