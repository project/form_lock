/**
 * @file
 * Defines Javascript behaviors for the Form Lock button.
 */
(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.formLock = {
    attach: function attach(context, drupalSettings) {
      if (!drupalSettings.form_lock) {
        return;
      }

      $.each(drupalSettings.form_lock, function (form_id, settings) {
        $('form#' + form_id, context)
          .once('form_lock')
          .each(function () {
            new Drupal.form_lock(this, settings);
          });
      });

      $('#drupal-modal').ajaxComplete(function() {
        $.each(drupalSettings.form_lock, function (form_id, settings) {
          $('form[id^="' + form_id +'"]', context)
            .once('form_lock')
            .each(function () {
              new Drupal.form_lock(this, settings);
            });
        });
      });
    }
  };

  Drupal.form_lock = function (form, settings) {
    var that = this;
    var ajaxCall = Drupal.ajax({
      url: settings.lockUrl,
      element: form
    });

    ajaxCall.commands.insert = function () {
      if (arguments[1].selector === '') {
        arguments[1].selector = '#' + form.id;
      }
      Drupal.AjaxCommands.prototype.insert.apply(this, arguments);
    };

    ajaxCall.commands.formLock = function (ajax, response, status) {
      if (response.lockable && response.lock !== true) {
        that.lock();
      }
    };

    ajaxCall.execute();

    this.lock = function () {
      var $form = $(form);
      $form.prop('disabled', true).addClass('is-disabled');
      $form.find('input, textarea, select').prop('disabled', true).addClass('is-disabled');
      $form.find('input[type=submit]').prop('disabled', true).addClass('is-disabled');
    };
  }
}(jQuery, Drupal, drupalSettings));